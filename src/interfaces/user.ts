export interface UserInfo {
  [x: string]: any
  name: string
  email: string
  roleId: number | null
  password: string
}
