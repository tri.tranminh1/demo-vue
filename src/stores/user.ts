import { defineStore } from 'pinia'
import axiosInstance from '@/services/axios'
import { type UserInfo } from '@/interfaces/user'

export const useUserStore = defineStore('user', {
  state: () => ({
    userList: [],
    userRoleList: []
  }),

  getters: {},

  actions: {
    async getUserList() {
      try {
        const response = await axiosInstance.get(`/admin/users`)
        this.userList = response.data
      } catch (error: any) {
        throw new Error(error)
      }
    },
    async addUser(params: { payload: UserInfo }) {
      try {
        const { payload } = params
        const response = await axiosInstance.post(`/admin/users`, payload)
        this.getUserList()
        return response
      } catch (error: any) {
        throw new Error(error)
      }
    },
    async editUser(params: { id: string; payload: UserInfo }) {
      try {
        const { id, payload } = params
        const response = await axiosInstance.put(`/admin/users/${id}`, payload)
        this.getUserList()
        return response
      } catch (error: any) {
        throw new Error(error)
      }
    },
    async deleteUser(params: { id: string }) {
      try {
        const { id } = params
        const response = await axiosInstance.delete(`/admin/users/${id}`)
        this.getUserList()
        return response
      } catch (error: any) {
        throw new Error(error)
      }
    },
    async getUserRole() {
      try {
        const response = await axiosInstance.get(`/admin/roles`)
        this.userRoleList = response.data
      } catch (error: any) {
        throw new Error(error)
      }
    }
  }
})
