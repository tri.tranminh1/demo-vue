import { defineStore } from 'pinia'

export const useSpinnerStore = defineStore('spinner', {
  state: () => ({
    spinner: false
  }),

  actions: {
    showSpinner() {
      this.spinner = true
    },
    hideSpinner() {
      this.spinner = false
    }
  }
})
