import { useSpinnerStore } from '@/stores/spinner'

import axios from 'axios'

const ROOT_API = 'https://nih-api.dev2.hdwebsoft.co/api/v1'
const token =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIwMjI3ZmFkMS01OWExLTQ2MjAtYjM5My0zN2RjYmM5ZWU0M2UiLCJpYXQiOiIwNi8yOS8yMDIzIDA5OjQ5OjE1IiwiVXNlcklkIjoiMSIsIkRpc3BsYXlOYW1lIjoiQWRtaW4gMSIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMDUvMDUvaWRlbnRpdHkvY2xhaW1zL25hbWUiOiJhZG1pbkBtYWlsaW5hdG9yLmNvbSIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcm9sZSI6IkFkbWluIiwiZXhwIjoxNjg4MTE4NTU1fQ.KizuNheYhGLMjXaEw-kZzjo46m4jZiAIEUnEXuCBrgQ'

const axiosInstance = axios.create({
  baseURL: ROOT_API,
  timeout: 600000
})

axiosInstance.interceptors.request.use(
  function (config) {
    const spinner = useSpinnerStore()
    spinner.showSpinner()
    config.headers.Authorization = 'Bearer ' + token
    return config
  },
  function (error) {
    const spinner = useSpinnerStore()
    spinner.hideSpinner()
    return Promise.reject(error)
  }
)

axiosInstance.interceptors.response.use(
  function (response) {
    const spinner = useSpinnerStore()
    spinner.hideSpinner()
    return response
  },
  function (error) {
    const spinner = useSpinnerStore()
    spinner.hideSpinner()
    return Promise.reject(error)
  }
)

export default axiosInstance
